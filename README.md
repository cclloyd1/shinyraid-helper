Tool to help host raids in servers like ShinyRaids


## Features
- Host G-Max Raids
- Host Rotating Raids

## Features to come
- Work as general purpose embed sender (custom templates)
- Add general shiny enhanced template
- Import saved settings/templates to use on another computer or share with friends
import React from 'react';
import './App.css';
import CssBaseline from "@material-ui/core/CssBaseline";
import MaterialUIApp from "./MateriaLUIApp";
import {ToastProvider} from "react-toast-notifications";
import {ThemeProvider} from '@material-ui/core/styles';
import MUIToastContainer from "./components/MUIToastContainer";
import MUIToast from "./components/MUIToast";
import theme from "./theme";
//import DiscordProvider from "./contexts/DiscordProvider";
import {BrowserRouter as Router} from 'react-router-dom';
//import TemplatesProvider from "./contexts/TemplatesProvider";
import TemplatesProvider from "./components/TemplatesProvider";
import SettingsProvider from "./contexts/SettingsProvider";

import 'typeface-roboto';

export const urlPattern = /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?$/

function App() {
    return (
        <ThemeProvider theme={theme}>
            <ToastProvider autoDismissTimeout={3000} autoDismiss={true} placement="top-center" components={{ ToastContainer: MUIToastContainer, Toast: MUIToast }}>
                <Router>
                    <SettingsProvider>
                        <TemplatesProvider>
                            <CssBaseline />
                            <MaterialUIApp />
                        </TemplatesProvider>
                    </SettingsProvider>
                </Router>
            </ToastProvider>
        </ThemeProvider>
    );
}

export default App;

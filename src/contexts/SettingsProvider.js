import React, {useContext, useEffect} from 'react';
const SettingsContext = React.createContext({});


export default function SettingsProvider({ children }) {

    const [state, setState] = React.useState({
        game: 'sword',
        baby: false,
        autosave: true,
    });


    useEffect(() => {
        const storedSettings = window.localStorage.getItem('user_settings');
        if (storedSettings !== null) {
            setState(JSON.parse(storedSettings));
        }
        else
            window.localStorage.setItem('user_settings', JSON.stringify(state));
    }, []);

    const updateSettings = value => {
        setState(value);
        window.localStorage.setItem('user_settings', JSON.stringify(value));
    }

    return (
        <SettingsContext.Provider value={{ state, updateSettings }}>
            {children}
        </SettingsContext.Provider>
    );
}


export const useSettings = () => {
    const ctx = useContext(SettingsContext);

    if (!ctx) {
        throw Error('The `useSettings` hook must be called from a descendent of the `SettingsProvider`.');
    }

    return {
        settings: ctx.state,
        setSettings: ctx.updateSettings,
        game: ctx.state.game,
        baby: ctx.state.baby,
    };
};
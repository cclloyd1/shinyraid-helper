
import React, {useContext, useEffect} from 'react';
//import { INITIAL_STATE } from "../state";
//import {AuthContext, useAuth} from "./AuthProvider";
import {templates as defaultTemplates} from '../resources/templates';
import {useToasts} from "react-toast-notifications";

const TemplatesContext = React.createContext({});

export default function TemplatesProvider({ children }) {

    const { addToast } = useToasts();

    const [templates, setTemplates] = React.useState(defaultTemplates);
    const [ready, setReady] = React.useState(false);
    const [userTemplates, setUserTemplates] = React.useState([]);
    const [currentTemplate, setCurrentTemplate] = React.useState({type: '', name: '', label: ''});


    const resetTemplate = () => {
        setCurrentTemplate({type: '', name: '', label: ''});
    }

    useEffect(() => {
        const storedTemplates = window.localStorage.getItem('user_templates');
        if (storedTemplates !== null)
            setUserTemplates(JSON.parse(storedTemplates));
        else
            setUserTemplates([]);
        setReady(true);
    }, []);

    const addTemplate = (template) => {
        const storage = window.localStorage;
        let storedTemplates = []

        // load templates from storage if they exist
        if (storage.getItem('user_templates') !== null)
            storedTemplates = JSON.parse(storage.getItem('user_templates'));

        // Check if name is used by default template
        for (let t of templates) {
            if (t.name === template.name) {
                addToast('Error adding template: NAME_NOT_UNIQUE', {appearance: 'error'});
                return false;
            }
        }

        // Check if name is used by user defined template
        if (storedTemplates.length > 0) {
            for (let t of storedTemplates) {
                if (t.name === template.name) {
                    addToast('Error adding template: NAME_NOT_UNIQUE', {appearance: 'error'});
                    return false;
                }
            }
        }

        // Add template to list and save to storage
        storedTemplates.push(template)
        storage.setItem('user_templates', JSON.stringify(storedTemplates));
        setUserTemplates([...storedTemplates]);
        return template;
    }

    const sanitizeTemplate = template => {
        let newTemplate = template;
        return newTemplate;
    }

    const updateTemplate = (template) => {
        let baseTemplate = template;
        // Fix improper values to be correctly formatted defaults (workaround) TODO: fix later
        if (template.json.den === 17) {
            addToast('Event den not allowed.', {appearance: 'error'});
            return;
        }
        //baseTemplate.json.species = parseInt(template.json.species);

        const storage = window.localStorage;
        let storedTemplates = [];
        if (storage.getItem('user_templates') !== null)
            storedTemplates = JSON.parse(storage.getItem('user_templates'));

        let newTemplates = [];

        for (let t of storedTemplates) {
            if (t.name === template.name)
                newTemplates.push(template);
            else
                newTemplates.push(t);
        }
        setUserTemplates(newTemplates);
        storage.setItem('user_templates', JSON.stringify(newTemplates));
        return template;
    }

    const loadUserTemplates = (newTemplates) => {
        window.localStorage.setItem('user_templates', JSON.stringify(newTemplates));
        setUserTemplates(newTemplates);
    }


    const changeTemplate = (template) => {
        if (typeof template === 'string') {
            for (let t of templates) {
                if (t.name === template) {
                    setCurrentTemplate(t);
                    return true;
                }
            }
            for (let t of userTemplates) {
                if (t.name === template) {
                    setCurrentTemplate(t);
                    return true;
                }
            }
        }
        else {
            setCurrentTemplate(template);
            return true;
        }
        addToast('Error switching templates', {appearance: 'error'});
        return false;
    }

    const removeTemplate = (id) => {

    }

    const getTemplate = (id) => {}
    const getTemplates = () => {
        const storage = window.localStorage;
        let storedTemplates = []
        if (storage.getItem('user_templates') !== null) {
            let stored = JSON.parse(storage.getItem('user_templates'));
            let storedEdited = [];
            for (let t of stored) {
                let tnew = t;
                //tnew.json.species = parseInt(tnew.json.species);
                tnew.json.species = `${t.json.species}`
                storedEdited.push(tnew);
            }
            storage.setItem('user_templates', JSON.stringify(storedEdited));
            return storedEdited;
        }
        else
            return [];
    }


    return (
        <TemplatesContext.Provider value={{
            templates,
            currentTemplate,
            changeTemplate,
            addTemplate,
            updateTemplate,
            removeTemplate,
            getTemplate,
            getTemplates,
            setUserTemplates,
            loadUserTemplates,
            userTemplates,
            resetTemplate,
        }}>
            {ready && children}
        </TemplatesContext.Provider>
    );
}


export const useTemplates = () => {
    const ctx = useContext(TemplatesContext);

    if (!ctx) {
        throw Error('The `useTemplates` hook must be called from a descendent of the `TemplatesProvider`.');
    }

    return {
        templates: ctx.templates,
        defaultTemplates: ctx.templates,
        currentTemplate: ctx.currentTemplate,
        setCurrentTemplate: ctx.changeTemplate,
        addTemplate: ctx.addTemplate,
        updateTemplate: ctx.updateTemplate,
        removeTemplate: ctx.removeTemplate,
        getTemplate: ctx.getTemplate,
        getTemplates: ctx.getTemplates,
        setUserTemplates: ctx.setUserTemplates,
        loadUserTemplates: ctx.loadUserTemplates,
        userTemplates: ctx.userTemplates,
        resetTemplate: ctx.resetTemplate,
    };
};
import React, {useContext, useEffect} from 'react';
//import { INITIAL_STATE } from "../state";
//import {AuthContext, useAuth} from "./AuthProvider";
import Discord from 'discord.js';


const DiscordContext = React.createContext({});

export default function DiscordProvider({ children }) {

    //const [state, setState] = React.useState(INITIAL_STATE.settings);
    const [client, setClient] = React.useState({});
    const [code, setCode] = React.useState(undefined);

    useEffect(() => {
        setClient(new Discord.Client());
    }, []);

    useEffect(() => {
        const savedCode = window.localStorage.getItem('code');
        if (savedCode)
            setCode(savedCode);
    }, []);

    return (
        <DiscordContext.Provider value={{ client, setClient, code, setCode }}>
            {children}
        </DiscordContext.Provider>
    );
}


export const useDiscord = () => {
    const ctx = useContext(DiscordContext);
    //const authCtx = useContext(AuthContext);

    if (!ctx) {
        throw Error('The `useDiscord` hook must be called from a descendent of the `DiscordProvider`.');
    }

    return {
        discord: ctx.client,
        setDiscord: ctx.setClient,
        code: ctx.code,
        setCode: ctx.setCode,
    };
};
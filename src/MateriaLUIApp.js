import React from 'react';
import NavTop from "./nav/NavTop";
import AppContent from "./AppContent";
import NavLeft from "./nav/NavLeft";

export default function MaterialUIApp() {
    return (
        <>
            <NavLeft/>
            <NavTop/>
            <AppContent/>
        </>
    );
}

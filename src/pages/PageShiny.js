import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {useForm} from "react-hook-form";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import FormTemplates from "../components/form/FormTemplates";
import Box from "@material-ui/core/Box";
import FormShiny from "../components/form/FormShiny";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
}));

export default function PageShiny() {
    const classes = useStyles();

    const form = useForm();

    return (
        <Box className={classes.root}>
            <Typography variant={'h3'}>Shiny Raid</Typography>
            <Divider className={classes.pageDivider}/>

            <FormTemplates templateData={form.watch()} templateType={'shiny'}/>

            <FormShiny form={form}/>
        </Box>
    );

}


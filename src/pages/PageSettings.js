import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import {useTemplates} from "../contexts/TemplatesProvider";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import PageTitle from "../components/PageTitle";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import Switch from "@material-ui/core/Switch";
import {useToasts} from "react-toast-notifications";
import {useSettings} from "../contexts/SettingsProvider";
import beautify from "json-beautify";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
    padBottom: {
        marginBottom: theme.spacing(1),
    },
    sectionGrid: {
        marginBottom: theme.spacing(4),
    },
    space: {
        '& > *': {
            margin: theme.spacing(1),
        }
    },
    hidden: {
        display: 'none',
    }
}));

export default function PageSettings() {
    const classes = useStyles();

    const { getTemplates, loadUserTemplates } = useTemplates();
    const { addToast } = useToasts();
    const { settings, setSettings } = useSettings();

    const [state, setState] = React.useState(settings);


    const handleSelectChange = name => event => {
        const newState = {...state, [name]: event.target.value}
        setState(newState);
        setSettings(newState);
    }

    const handleCheckChange = (event) => {
        console.log('event.target.value', event.target.checked);
        const newState = {...state, [event.target.name]: event.target.checked}
        setState(newState);
        setSettings(newState);
    }

    const exportSettings = () => {
        let exportedSettings = {};
        exportedSettings.version = 1;

        exportedSettings.settings = state;
        exportedSettings.userTemplates = getTemplates();

        const exportedString = beautify(exportedSettings, null, 2, 80);

        let file = new Blob([exportedString], {type: 'text/plain'});
        if (window.navigator.msSaveOrOpenBlob) // IE10+
            window.navigator.msSaveOrOpenBlob(file, 'shinyraid_exported_settings.txt');
        else { // Others
            let a = document.createElement("a"),
                url = URL.createObjectURL(file);
            a.href = url;
            a.download = 'shinyraid_exported_settings.json';
            document.body.appendChild(a);
            a.click();
            setTimeout(function() {
                document.body.removeChild(a);
                window.URL.revokeObjectURL(url);
            }, 0);
        }
        //copy(JSON.stringify(exportedString));
    }

    const importSettings = () => {
        let element = document.getElementById('import-settings-input');
        element.click();
    }

    const handleFileChange = () => {
        const reader = new FileReader();
        reader.onload = e => {
            const text = e.target.result;
            let settings;
            try {
                settings = JSON.parse(text);
            }
            catch (err) {
                console.error('Error parsing settings', err);
                addToast('Error parsing settings', {appearance: 'error'});
                return false;
            }
            console.log('settings', settings);
            loadUserTemplates(settings.userTemplates);
            setSettings(settings.settings);
        };
        reader.readAsText(document.getElementById('import-settings-input').files[0])
    }

    useEffect(() => {
        document.title = `EmbedHelper · Settings`;
    }, [])

    return (
        <Paper className={classes.root}>
            <PageTitle title={'Settings'} />

            <Grid container spacing={2}>
                <Grid item xs={12} className={classes.description}>
                    <Typography variant={'body1'}>Here you can export and import settings to be backed up or to use on another computer/browser.  If you have a lot of templates you would like to be saved, it is recommended to back them up occasionally.  </Typography>
                </Grid>

                <Grid item xs={6} md={4}>
                    <Typography variant={'h6'}>Game: </Typography>
                </Grid>
                <Grid item xs={6}>
                    <FormControl className={classes.margin}>
                        <Select
                            labelId="game-select-label"
                            id="game-select"
                            value={state.game}
                            onChange={handleSelectChange('game')}
                            defaultValue={'sword'}
                            input={<Input />}
                        >
                            <MenuItem value={'sword'}>Sword</MenuItem>
                            <MenuItem value={'shield'}>Shield</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
                <Grid item xs={6} md={4}>
                    <Typography variant={'h6'}>Show Babies?</Typography>
                </Grid>
                <Grid item xs={6}>
                    <Switch
                        checked={state.baby}
                        onChange={handleCheckChange}
                        name={'baby'}
                    />
                </Grid>
                <Grid item xs={6} md={4}>
                    <Typography variant={'h6'}>Autosave</Typography>
                </Grid>
                <Grid item xs={6}>
                    <Switch
                        checked={state.autosave}
                        onChange={handleCheckChange}
                        name={'autosave'}
                        disabled
                    />
                </Grid>

                <Grid item xs={12} className={classes.space}>
                    <Button onClick={exportSettings} variant={'contained'} color={'primary'}>Export</Button>
                    <Button onClick={importSettings} variant={'contained'} color={'primary'}>Import</Button>
                    <Input
                        type={'file'}
                        name={'import-settings'}
                        id={'import-settings-input'}
                        className={classes.hidden}
                        onChange={handleFileChange}
                    />
                </Grid>
            </Grid>

        </Paper>
    );

}


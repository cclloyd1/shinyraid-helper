import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {useForm} from "react-hook-form";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import FormTemplates from "../components/form/FormTemplates";
import Box from "@material-ui/core/Box";
import FormGmax from "../components/form/FormGmax";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
}));

export default function PageGmax() {
    const classes = useStyles();

    const form = useForm();

    return (
        <Box className={classes.root}>
            <Typography variant={'h3'}><img src={'gigantamax.png'} className={classes.titleImage}  alt={'gigantamax'}/> G-Max Den</Typography>
            <Divider className={classes.pageDivider}/>

            <FormTemplates templateData={form.watch()} templateType={'gmax'}/>

            <FormGmax form={form}/>
        </Box>
    );

}


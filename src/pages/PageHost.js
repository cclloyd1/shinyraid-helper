import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import FormCustom from "../components/form/FormCustom";
//import {useTemplates} from "../contexts/TemplatesProvider";
import {useTemplates} from "../components/TemplatesProvider";
import PageRotating from "./PageRotating";
import PageGmax from "./PageGmax";
import PageShiny from "./PageShiny";
import {Redirect} from "react-router-dom";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
}));

export default function PageHost() {
    const classes = useStyles();

    const { currentTemplate } = useTemplates();


    useEffect(() => {
        if (currentTemplate.type === '')
            document.title = `EmbedHelper · Host`;
    }, [currentTemplate])

    return (
        <Paper className={classes.root}>
            {currentTemplate.type === '' && <Redirect to="/" />}

            {currentTemplate.type === 'gmax' && <PageGmax/>}
            {currentTemplate.type === 'rotating' && <PageRotating/>}
            {currentTemplate.type === 'shiny' && <PageShiny/>}
            {currentTemplate.type === 'custom' && <FormCustom/>}
        </Paper>
    );

}


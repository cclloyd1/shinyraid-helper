import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {useForm} from "react-hook-form";
import FormRotating from "../components/form/FormRotating";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import FormTemplates from "../components/form/FormTemplates";
import Box from "@material-ui/core/Box";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
}));

export default function PageRotating() {
    const classes = useStyles();

    const form = useForm();

    return (
        <Box className={classes.root}>
            <Typography variant={'h3'}>Rotating Den</Typography>
            <Divider className={classes.pageDivider}/>

            <FormTemplates templateData={form.watch()} templateType={'rotating'}/>

            <FormRotating form={form}/>
        </Box>
    );

}


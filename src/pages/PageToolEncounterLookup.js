import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import PageTitle from "../components/PageTitle";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
}));

export default function PageToolEncounterLookup() {
    const classes = useStyles();

    useEffect(() => {
        document.title = `EmbedHelper · Encounter Lookup`;
    }, [])

    return (
        <Paper className={classes.root}>
            <PageTitle title={'Encounter Lookup'} />

            <div>Work in progress.</div>
        </Paper>
    );

}


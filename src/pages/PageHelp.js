import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import FormCustom from "../components/form/FormCustom";
import {useTemplates} from "../contexts/TemplatesProvider";
import PageRotating from "./PageRotating";
import PageGmax from "./PageGmax";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import clsx from "clsx";
import PageShiny from "./PageShiny";
import {templates} from "../resources/templates";
import PageTitle from "../components/PageTitle";
import FAQItem from "../components/FAQItem";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
    divider: {
        marginBottom: theme.spacing(2),
    },
    mainHelp: {
        margin: theme.spacing(3, 0),
        //padding: theme.spacing(2),
    },
    faqinfo: {
        marginBottom: theme.spacing(4),
    }
}));

export default function PageHelp() {
    const classes = useStyles();

    return (
        <Paper className={classes.root}>
            <PageTitle title={'Help'}/>

            <Box className={classes.mainHelp}>
                <Typography variant={'body1'}>This page contains various resources to help understand how to use the site and what each thing does.</Typography>
            </Box>

            <Typography variant={'h3'} gutterBottom>FAQ</Typography>

            <Typography variant={'subtitle2'} className={classes.faqinfo}>Have a question to add?  Message @cclloyd on the discord server.</Typography>

            <FAQItem
                question={'What is this site?'}
                answer={'This is a tool to make it easier to use embeds for your shiny raid.  Otherwise, you would have to figure out where to put each field in the embed to keep it concise and have it look good.'}
            />

            <FAQItem
                question={'What is an embed?'}
                answer={'An embed is a type of message normally only allowed to discord bots.  By sending it through a webhook, you can allow users to send them.  These allow for messages with formatting and images that can look much nicer than normal code blocks that are usually used.'}
            />

            <FAQItem
                question={'What type of raid should I choose?'}
                answer={<div>
                    There are 3 types of raid templates available
                    <ul>
                        <li><b>G-Max:</b> This is for the G-Max pokemon in the game.  It comes with the G-Max icon included and auto fills the images with their shiny gmax form.</li>
                        <li><b>Rotating:</b> This is for when you are hosting a single den with rotating mons, before you have locked in the den.</li>
                        <li><b>Shiny:</b> This is a general purpose template.  It works for shiny and non-shiny pokemon.  This is the most used template.</li>
                    </ul>
                </div>}
            />

            <FAQItem
                question={'Why is the image optional?'}
                answer={'By default, it will fill images from Serebii, including shiny and G-Max images, so you don\'t have to do anything!  You can however, fill this in with an image URL of your choice, in the event you want something like a gif image.'}
            />

            <FAQItem
                question={'Why is custom title optional?'}
                answer={'The title is generated based on what type of raid you are hosting.  I allow for you to use a custom title in the event you want to get fun with your raids.  If you choose prepend/append instead of "replace" for the title, it will put your custom title before or after the generated title.'}
            />

            <FAQItem
                question={'Where can I change my game?'}
                answer={'In the settings page.  Here you can change between Sword/Shield, and also whether to show baby pokemon or not. (If you completed your game, you want to set this to off)'}
            />

            <FAQItem
                question={'What is import/export in the settings?'}
                answer={'This allows you to export your settings to a file as a backup in case anything ever goes wrong on my end, or just to copy to another computer to use.  It also exports all your templates too.  Import is the same but you must load a settings file you saved beforehand.  This file can be edited by hand if you know what you\'re doing, as it\'s standard JSON.'}
            />

            <FAQItem
                question={'Why are the den numbers different than Serebii?'}
                answer={'The first dropdown are the encounter numbers.  These are the same as RaidFinder, and each one is a unique beam location.  The number in parentheses in the rarity column is the Serebii den number, as these are not unique and some are shared between dens.'}
            />

            <FAQItem
                question={'How do I get my Discord ID?'}
                answer={'Your discord ID is a long string of numbers (NOT your @ tag).  To get it, you must go to settings in Discord and enable Developer Mode.  After you do this, you can right-click (or hold on mobile) and copy your ID when you view yourself in the side of any discord server.'}
            />

            <FAQItem
                question={'Why do I need my Discord ID?'}
                answer={'The message isn\'t being sent by you, but by a bot.  This helps to identify who in the discord is doing the raid, and to make it easier for them to ping you.'}
            />


        </Paper>
    );

}


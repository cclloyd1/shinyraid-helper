import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
//import {useTemplates} from "../contexts/TemplatesProvider";
import {useTemplates} from "../components/TemplatesProvider";

import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import clsx from "clsx";
import {templates} from "../resources/templates";
import { Redirect }from "react-router-dom";


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
    padBottom: {
        marginBottom: theme.spacing(1),
    },
    sectionGrid: {
        marginBottom: theme.spacing(4),
    },
    space: {
        '& > *': {
            margin: theme.spacing(1),
        }
    },
    speciesIcon: {
        height: 28,
        width: 'auto',
        marginRight: theme.spacing(1.5),
    },
}));

export default function PageHome() {
    const classes = useStyles();

    const { currentTemplate, userTemplates, setCurrentTemplate } = useTemplates();


    const changeTemplate = (template) => {
        setCurrentTemplate(template);
    }

    const handleNewTemplate = (type) => {
        let base;
        for (let t of templates) {
            if (t.type === type) {
                base = t;
            }
        }
        base.new = true;
        setCurrentTemplate(base);
    }

    useEffect(() => {
        if (currentTemplate.type === '')
            document.title = `EmbedHelper · Home`;
    }, [currentTemplate])

    return (
        <Paper className={classes.root}>
            {currentTemplate.type !== '' && <Redirect to="/host" />}

            <Grid container spacing={2}>
                <Grid item xs={12} className={classes.sectionGrid}>
                    <Typography variant={'h4'}>Welcome to ShinyRaid's Embed Helper</Typography>
                    <Divider className={classes.padBottom}/>
                    <Typography variant={'body1'}>To get started, either choose a template type to base your raid off of, or choose a saved template you have.</Typography>
                </Grid>

                <Grid item xs={12}>
                    <Typography variant={'h5'}>Choose a template type</Typography>
                    <Divider/>
                </Grid>
                <Grid item xs={12} className={clsx(classes.sectionGrid, classes.space)}>
                    <Button variant={'contained'} color={'primary'} onClick={() => {handleNewTemplate('gmax')}}>G-Max Raid</Button>
                    <Button variant={'contained'} color={'primary'} onClick={() => {handleNewTemplate('rotating')}}>Rotating Den</Button>
                    <Button variant={'contained'} color={'primary'} onClick={() => {handleNewTemplate('shiny')}}>Shiny Raid</Button>
                </Grid>

                <Grid item xs={12}>
                    <Typography variant={'h5'}>Saved Templates</Typography>
                    <Divider/>
                </Grid>
                <Grid item xs={12}  className={classes.space}>
                    <Typography variant={'h6'}>G-Max Raids</Typography>
                    {userTemplates.map(n =>
                        n.type === 'gmax' &&
                        <Button key={n.name} variant={'contained'} color={'primary'} onClick={() => {changeTemplate(n)}}>
                            <img src={`https://www.serebii.net/pokedex-swsh/icon/${n.json.species.toString().padStart(3, '0')}-gi.png`} alt={''} className={classes.speciesIcon}/> {n.label}
                        </Button>
                    )}
                </Grid>
                <Grid item xs={12}  className={classes.space}>
                    <Typography variant={'h6'}>Rotating Den</Typography>
                    {userTemplates.map(n =>
                        n.type === 'rotating' &&
                        <Button key={n.name} variant={'contained'} color={'primary'} onClick={() => {changeTemplate(n)}}>
                            <img src={`https://www.serebii.net/pokedex-swsh/icon/${n.json.species.toString().padStart(3, '0')}.png`} alt={''} className={classes.speciesIcon}/> {n.label}
                        </Button>
                    )}
                </Grid>
                <Grid item xs={12}  className={classes.space}>
                    <Typography variant={'h6'}>Shiny Raid</Typography>
                    {userTemplates.map(n =>
                        n.type === 'shiny' &&
                        <Button key={n.name} variant={'contained'} color={'primary'} onClick={() => {changeTemplate(n)}}>
                            <img src={`https://www.serebii.net/pokedex-swsh/icon/${n.json.species.toString().padStart(3, '0')}.png`} alt={''} className={classes.speciesIcon}/> {n.label}
                        </Button>
                    )}
                </Grid>
                <Grid item xs={12}  className={classes.space}>
                    <Typography variant={'h6'}>Custom</Typography>
                    {userTemplates.map(n =>
                        n.type === 'custom' &&
                        <Button key={n.name} variant={'contained'} color={'primary'} onClick={() => {changeTemplate(n)}}>
                            <img src={`https://www.serebii.net/pokedex-swsh/icon/${n.json.species.toString().padStart(3, '0')}.png`} alt={''}/>{n.label}
                        </Button>
                    )}
                </Grid>
                <Grid item xs={12} className={classes.sectionGrid}> </Grid>
            </Grid>
        </Paper>
    );

}


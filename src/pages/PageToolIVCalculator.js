import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import PageTitle from "../components/PageTitle";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
        marginBottom: theme.spacing(4),
    },
}));

export default function PageToolIVCalculator() {
    const classes = useStyles();

    useEffect(() => {
        document.title = `EmbedHelper · IV Calculator`;
    }, [])

    return (
        <Paper className={classes.root}>
            <PageTitle title={'IV Calculator'} />

            <div>Work in progress.</div>
        </Paper>
    );

}


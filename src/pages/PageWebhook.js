import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {Redirect, useLocation} from "react-router-dom";
import {useDiscord} from "../contexts/DiscordProvider";

const useStyles = makeStyles(theme => ({
    root: {

    },
}));

function getJsonFromUrl(url) {
    let query = url.substr(1);
    let result = {};
    query.split("&").forEach(function(part) {
        let item = part.split("=");
        result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
}

const serialize = function(obj) {
    let str = [];
    for (let p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}

export default function PageWebhook() {
    const classes = useStyles();

    const location = useLocation();
    const [params, setParams] = React.useState(getJsonFromUrl(location.search));
    const {code, setCode} = useDiscord();
    const [redirect, setRedirect] = React.useState(false);

    useEffect(() => {
        if (params.code) {
            setCode(params.code);
            console.log('code', params.code);
            window.localStorage.setItem('code', params.code);

            let newParams = {
                'client_id': "715023697244717107",
                'client_secret': 'A56hpCLYGvXADi1RdD9GdNxJkQGxsyUM',
                'grant_type': 'authorization_code',
                'code': params.code,
                'scope': 'webhook.incoming',
                'redirect_uri': 'http://localhost:3000/webhook',
            }

            const url = `https://discord.com/api/oauth2/token`;
            fetch(url, {
                method: 'POST',
                body: new URLSearchParams(newParams),
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            }).then(res => {
                try {
                    return res.json();
                }
                catch {
                    throw 'error';
                }
            }).then(data => {
                console.log('data', data);
            }).catch(err => {
                console.error('error', err);
            });
        }
    }, [params]);

    useEffect(() => {
        if (code) {
             setRedirect(true);
        }
    }, [code]);

    return (
        <>
            <div>Webhook</div>
            <div>{code}</div>
            {redirect && false && <Redirect to='/' />}
        </>
    );
}


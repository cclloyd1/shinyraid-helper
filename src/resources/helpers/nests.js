/*
Source:
https://raw.githubusercontent.com/Admiral-Fish/RaidFinder/master/Resources/Encounters/nests.json

Convert json to javascript object with TableID as object keys.

Run these with node.
*/

const fs = require('fs');

const rawdata = fs.readFileSync('./src/resources/helpers/nests.json');
const encounters = JSON.parse(rawdata);

newEncounters = {}

for (let e of encounters.Tables) {
    newEncounters[e.TableID] = e;
    //break;
}

console.log(JSON.stringify(newEncounters));

// Outputs the nests in JSON.  Use tool to convert to javascript object and paste it in resources/nests.json


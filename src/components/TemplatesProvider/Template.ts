
export type TemplateType = 'gmax' | 'rotating' | 'shiny' | 'custom' | ''

export interface TemplateValues {
    webhook?: string;
    username?: string;
    avatarURL?: string;
    discordID?: string;
    embedColor?: string;
    ivhp?: number;
    ivatk?: number;
    ivdef?: number;
    ivspatk?: number;
    ivspdef?: number;
    ivspe?: number;
    den?: number;
    rare?: boolean;
    nature?: string;
    ability?: string;
    ha?: boolean;
    gender?: string;
    star?: number;
    embedTitle?: string;
    species?: number;
    speciesURL?: string;
    shiny?: string;
    code?: string;
    sn?: string;
    ign?: string;
    fc?: string;
    notes?: string;
}

export interface Template {
    name: string;
    label: string;
    type: TemplateType;
    default: boolean;
    json: TemplateValues;
    version: number;
}


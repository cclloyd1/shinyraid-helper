import * as React from 'react';
import {templates as defaultTemplates} from '../../resources/templates';
// @ts-ignore
import {useToasts} from "react-toast-notifications";
import {Template} from "./Template";
import {useContext, useEffect} from "react";


// TODO: Define context type
const TemplatesContext = React.createContext({} as any);

export default function TemplatesProvider(props: any) {
    const { children } = props;

    const blankTemplate = {
        name: '',
        type: '',
        label: '',
        version: 2,
        json: {},
        default: true,
    } as Template;

    const { addToast } = useToasts();

    const [ready, setReady] = React.useState<boolean>(false);
    const [templates] = React.useState(defaultTemplates);
    const [userTemplates, setUserTemplates] = React.useState<Template[]>([]);
    const [current, setCurrent] = React.useState<Template>(blankTemplate);

    const resetTemplate = () => {
        setCurrent(blankTemplate);
    }

    useEffect(() => {
        const storedTemplates = window.localStorage.getItem('user_templates');
        if (storedTemplates !== null)
            setUserTemplates(JSON.parse(storedTemplates));
        else
            setUserTemplates([]);
        setReady(true);
    }, []);


    const addTemplate = (template: Template) => {
        const storage = window.localStorage;
        let storedTemplates = [];

        // load templates from storage if they exist
        let storedStr = storage.getItem('user_templates');
        if (storedStr !== null)
            storedTemplates = JSON.parse(storedStr);

        // Check if name is used by default template
        for (let t of templates) {
            if (t.name === template.name) {
                addToast('Error adding template: NAME_NOT_UNIQUE', {appearance: 'error'});
                return false;
            }
        }

        // Check if name is used by user defined template
        if (storedTemplates.length > 0) {
            for (let t of storedTemplates) {
                if (t.name === template.name) {
                    addToast('Error adding template: NAME_NOT_UNIQUE', {appearance: 'error'});
                    return false;
                }
            }
        }

        // Add template to list and save to storage
        storedTemplates.push(template)
        storage.setItem('user_templates', JSON.stringify(storedTemplates));
        setUserTemplates([...storedTemplates]);
        return template;
    }

    const sanitizeTemplate = (template: Template) => {
        let newTemplate = template;
        return newTemplate;
    }

    const updateTemplate = (template: Template) => {
        const storage = window.localStorage;
        let baseTemplate = template;
        let storedTemplates = [];

        // Fix improper values to be correctly formatted defaults (workaround) TODO: fix later
        if (template.json.den === 17) {
            addToast('Event den not allowed.', {appearance: 'error'});
            return;
        }

        let stored = storage.getItem('user_templates');
        if (stored !== null)
            storedTemplates = JSON.parse(stored);


        let newTemplates = [];
        for (let t of storedTemplates) {
            if (t.name === template.name)
                newTemplates.push(template);
            else
                newTemplates.push(t);
        }
        setUserTemplates(newTemplates);
        storage.setItem('user_templates', JSON.stringify(newTemplates));
        return template;
    }

    const changeTemplate = (template: Template) => {
        setCurrent(template);
        return true;
    }

    const removeTemplate = (id: number) => {}

    const getTemplate = (id: number) => {}


    return (
        <TemplatesContext.Provider value={{
            templates,
            userTemplates,
            current,
            changeTemplate,
            addTemplate,
            updateTemplate,
            removeTemplate,
            resetTemplate,
        }}>
            {ready && children}
        </TemplatesContext.Provider>
    );
}


export const useTemplates = () => {
    const ctx = useContext(TemplatesContext);

    if (!ctx) {
        throw Error('The `useTemplates` hook must be called from a descendent of the `TemplatesProvider`.');
    }

    return {
        templates: ctx.templates,
        defaultTemplates: ctx.templates,
        userTemplates: ctx.userTemplates,
        currentTemplate: ctx.current,
        setCurrentTemplate: ctx.changeTemplate,
        addTemplate: ctx.addTemplate,
        updateTemplate: ctx.updateTemplate,
        removeTemplate: ctx.removeTemplate,
        resetTemplate: ctx.resetTemplate,
    };
};
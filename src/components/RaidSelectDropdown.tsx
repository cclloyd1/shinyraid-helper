import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import {useTemplates} from "./TemplatesProvider";
import InputLabel from "@material-ui/core/InputLabel";
import FilledInput from "@material-ui/core/FilledInput";
import {Template} from "./TemplatesProvider/Template";


const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: -5,
    },
    templateSelect: {
        color: '#fff !important',
    },
}));

export default function RaidSelectDropdown() {
    const classes = useStyles();

    const { currentTemplate, defaultTemplates, userTemplates, setCurrentTemplate } = useTemplates();

    const handleChange = async (event: any) => {
        await setCurrentTemplate(event.target.value);
    };


    return (
        <FormControl variant="filled" className={classes.root}>
            <InputLabel htmlFor="template-select" classes={{focused: classes.templateSelect}}>Raid Template</InputLabel>
            <Select
                native
                className={classes.templateSelect}
                name={'template'}
                label={'Raid Template'}
                variant={'filled'}
                margin={'dense'}
                value={currentTemplate.name}
                onChange={handleChange}
                input={<FilledInput disableUnderline/>}
            >
                <option value={''} disabled> </option>
                <optgroup label="Default Templates">
                    {defaultTemplates.map((n: Template) =>
                        <option key={n.name} value={n.name}>New {n.label}</option>
                    )}
                </optgroup>
                <optgroup label="G-Max Templates">
                    {userTemplates.map((n: Template) =>
                        n.type === 'gmax' && <option key={n.name} value={n.name}>{n.label}</option>
                    )}
                </optgroup>
                <optgroup label="Rotating Templates">
                    {userTemplates.map((n: Template) =>
                        n.type === 'rotating' && <option key={n.name} value={n.name}>{n.label}</option>
                    )}
                </optgroup>
                <optgroup label="Shiny Templates">
                    {userTemplates.map((n: Template) =>
                        n.type === 'shiny' && <option key={n.name} value={n.name}>{n.label}</option>
                    )}
                </optgroup>
            </Select>
        </FormControl>
    );
}

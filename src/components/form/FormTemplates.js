import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import {useToasts} from "react-toast-notifications";
import Grid from "@material-ui/core/Grid";
import {useForm} from "react-hook-form";
import EmbedTextField from "../EmbedTextField";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
//import {useTemplates} from "../../contexts/TemplatesProvider";
import {useTemplates} from "../TemplatesProvider";
import Tooltip from "@material-ui/core/Tooltip";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Box from "@material-ui/core/Box";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import clsx from "clsx";


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
    pageDivider: {
        marginBottom: theme.spacing(3),
    },
    titleImage: {
        marginRight: theme.spacing(1),
    },
    sectionGrid: {
        marginBottom: theme.spacing(3),
        marginTop: theme.spacing(2),
    },
    centerCheckbox: {
        display: 'flex',
    },
    icon: {
        color: theme.palette.text.primary,
    },
    currentTemplateHeader: {
        display: 'inline-block',
        marginRight: theme.spacing(1),
        fontSize: 24,
    },
    currentTemplate: {
        display: 'inline-block',
        marginRight: theme.spacing(2),
        fontSize: 24,
    },
    button: {
        marginLeft: theme.spacing(1),
    },
    menuContent: {
        padding: 0,
    },
    menuContentBox: {
        padding: theme.spacing(2),
        minWidth: 400,
    },
    centerGrid: {
        display: 'flex',
        alignItems: 'center',
    },
    dialogTitle: {
        fontSize: 32,
    },
    hidden: {
        display: 'none',
    }
}));

export default function FormTemplates(props) {
    const classes = useStyles();
    const { templateData } = props;

    const { addToast } = useToasts();
    const { currentTemplate, setCurrentTemplate, addTemplate, updateTemplate, resetTemplate } = useTemplates();
    const { register, handleSubmit, errors, triggerValidation, watch, setValue, getValues } = useForm({
        validateCriteriaMode: 'onChange',
    });

    const [saveOpen, setSaveOpen] = React.useState(false);

    const toggleSaveOpen = () => {
        setSaveOpen(!saveOpen);
    }

    const cancelNewTemplate = () => {
        resetTemplate();
    }

    const saveTemplate = (data, e) => {
        const values = watch();
        let template = {
            name: values.name,
            label: values.label,
            type: values.type,
            default: false,
            json: templateData,
        }
        const newTemplate = addTemplate(template);
        if (newTemplate) {
            toggleSaveOpen();
            addToast(`Saved template ${template.label}!`);
            setCurrentTemplate(newTemplate);
        }
    }

    const handleUpdateTemplate = () => {
        triggerValidation().then();
        const values = getValues();
        let template = {
            name: values.name,
            label: values.label,
            type: values.type,
            default: false,
            json: templateData,
        }
        const newTemplate = updateTemplate(template);
        if (newTemplate) {
            toggleSaveOpen();
            addToast(`Updated template ${newTemplate.label}!`);
            setCurrentTemplate(newTemplate);
        }
    }

    useEffect(() => {
        if (currentTemplate.default) {
            toggleSaveOpen();
        }
        setValue([
            {name: currentTemplate.name},
            {label: currentTemplate.label},
            {type: currentTemplate.type},
        ])
    }, [currentTemplate]);



    return (
        <form onSubmit={handleSubmit(saveTemplate)}>

            <Dialog open={saveOpen} onBackdropClick={toggleSaveOpen}>
                <DialogTitle className={classes.dialogTitle}>Save Template</DialogTitle>
                <DialogContent className={classes.menuContent}>
                    <Divider/>
                    <Box className={classes.menuContentBox}>
                        <Grid container spacing={2}>
                            <Grid item xs={6}>
                                <EmbedTextField
                                    name={'name'}
                                    label={'Template ID'}
                                    defaultValue={currentTemplate.default ? `${currentTemplate.name}2` : currentTemplate.name}
                                    helperText={'Unique ID.  Lowercase, no spaces or numbers.'}
                                    error={errors.name !== undefined}
                                    inputRef={register({required: true, pattern: /^[A-Za-z0-9]+$/})}
                                />
                            </Grid>
                            <Grid item xs={6}>
                                <EmbedTextField
                                    name={'label'}
                                    label={'Template Name'}
                                    defaultValue={currentTemplate.default ? `${currentTemplate.label} 2` : currentTemplate.label}
                                    helperText={'User-friendly name for template.  Can contain spaces.'}
                                    error={errors.name !== undefined}
                                    inputRef={register({required: true, pattern: /[A-Za-z0-9 _\-+]+/})}                                />
                            </Grid>
                            <Grid item xs={6}>
                                <EmbedTextField select SelectProps={{native: true}}
                                    name={'type'}
                                    label={'Template Type'}
                                    defaultValue={currentTemplate.type}
                                    disabled
                                    inputRef={register({})}
                                >
                                    <option value={'gmax'}>Gigantamax</option>
                                    <option value={'rotating'}>Rotating</option>
                                    <option value={'shiny'}>Shiny</option>
                                    <option value={'custom'}>Custom</option>
                                </EmbedTextField>
                            </Grid>
                        </Grid>
                    </Box>
                    <Divider/>
                </DialogContent>
                <DialogActions>
                    {currentTemplate.default === false && <Button onClick={handleUpdateTemplate}>Update</Button>}
                    <Button onClick={saveTemplate} type={'submit'}>Save New</Button>
                    <Button onClick={cancelNewTemplate} className={clsx({
                        [classes.hidden]: !currentTemplate.default,
                    })}>Cancel</Button>
                    <Button onClick={toggleSaveOpen} className={clsx({
                        [classes.hidden]: currentTemplate.default,
                    })}>Close</Button>
                </DialogActions>
            </Dialog>


            <Grid container spacing={2} className={classes.sectionGrid}>
                <Grid item xs={12} className={classes.centerGrid}>
                    <Typography variant={'h6'} className={classes.currentTemplateHeader}>Current Template: </Typography>
                    <Typography variant={'body1'} className={classes.currentTemplate}> {currentTemplate.label}</Typography>
                    <Tooltip title={'Save template and values'}>
                        <Button onClick={toggleSaveOpen} color={'primary'} variant={'contained'} className={classes.button}>Save Template</Button>
                    </Tooltip>
                </Grid>
            </Grid>
        </form>
    );
}


import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {useToasts} from "react-toast-notifications";
import Discord from 'discord.js';
import Grid from "@material-ui/core/Grid";
import EmbedTextField from "../EmbedTextField";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import {species} from "../../resources/species";
//import {useTemplates} from "../../contexts/TemplatesProvider";
import {useTemplates} from "../TemplatesProvider";
import FormFragmentEmbed from "./fragments/FormFragmentEmbed";
import FormFragmentHost from "./fragments/FormFragmentHost";
import FormFragmentBaseSection from "./fragments/FormFragmentBaseSection";
import FormFieldEmbedTitle from "./fields/FormFieldEmbedTitle";
import FormFieldIVs from "./fields/FormFieldIVs";
import FormFieldNature from "./fields/FormFieldNature";
import FormFieldShiny from "./fields/FormFieldShiny";
import FormFieldGender from "./fields/FormFieldGender";
import FormFieldHA from "./fields/FormFieldHA";
import FormFieldSpeciesURL from "./fields/FormFieldSpeciesURL";
import FormFieldStarCount from "./fields/FormFieldStarCount";
import FormFieldSubmitButton from "./fields/FormFieldSubmitButton";
import {getFullNature} from "../../helpers/nature";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
    pageDivider: {
        marginBottom: theme.spacing(3),
    },
}));

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}



export default function FormShiny(props) {
    const { addToast } = useToasts();
    const { currentTemplate } = useTemplates();
    const { register, handleSubmit, setValue } = props.form;

    const [state] = React.useState(currentTemplate.json);
    const classes = useStyles();


    // Send embed through webhook URL
    const sendEmbed = (data, e) => {
        let friendInfo = '';
        if (data.sn === data.ign)
            friendInfo += `**SN/IGN:** ${data.sn} │ `;
        else
            friendInfo += `**IGN:** ${data.ign} │ **SN:** ${data.sn} │ `;
        friendInfo += `${data.fc}`;

        let gender = '';
        if (data.gender === 'male')
            gender = '♂ │';
        else if (data.gender === 'female')
            gender = '♀ │';

        let nature = '';
        if (data.nature !== 'none')
            nature = `${getFullNature(data.nature)} │ `;

        let monURL;
        if (data.shiny === 'not')
            monURL = `https://www.serebii.net/swordshield/pokemon/${data.species.toString().padStart(3, '0')}.png`
        else
            monURL = `https://www.serebii.net/Shiny/SWSH/${data.species.toString().padStart(3, '0')}.png`
        if (data.speciesURL !== '')
            monURL = data.speciesURL;

        let notes = ''
        if (data.discordID !== '')
            notes = `**Discord:** <@!${data.discordID}>\n`;
        notes += data.notes;

        let shiny = ''
        if (data.shiny === 'star')
            shiny = 'https://i.imgur.com/HNLNdb2.png';
        else if (data.shiny === 'square')
            shiny = 'https://i.imgur.com/VNXgbMu.png';

        let title = `${species[data.species-1]}`;
        if (data.embedTitle !== '') {
            if (data.embedTitleAppend === 'append')
                title = `${data.embedTitle} ${title}`;
            else if (data.embedTitleAppend === 'prepend')
                title = `${title} ${data.embedTitle}`;
            else title = data.embedTitle;
        }

        let ha = '';
        if (data.ha)
            ha = '(HA)';


        const embed = new Discord.MessageEmbed()
        embed.setAuthor(title, shiny);
        embed.setDescription(`${'⭐'.repeat(data.star)} │ ${gender} ${capitalize(data.shiny)} Shiny`);
        embed.setThumbnail(monURL);
        embed.setColor(data.embedColor);
        embed.addField(`${data.ivhp} / ${data.ivatk} / ${data.ivdef} / ${data.ivspatk} / ${data.ivspdef} / ${data.ivspe} │ ${nature}${data.ability} ${ha}`,
            `Code: ${data.code}`,
            false
        );
        embed.addField(friendInfo, notes, false );


        fetch(data.webhook, {
            method: 'POST',
            body: JSON.stringify({
                username: data.username,
                avatar_url: data.avatarURL,
                embeds: [embed.toJSON()],
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(res => {
            if (res.ok)
                addToast('Sent embed!');
            else
                addToast('Error sending embed!', {appearance: 'error'});
        })
    }

    // Update form values when template changes
    useEffect(() => {
        let values = [];
        for (let key in currentTemplate.json) {
            values.push({[key]: currentTemplate.json[key]});
        }
        setValue(values);
        document.title = `EmbedHelper · ${currentTemplate.label}`;
    }, [currentTemplate]);

    return (
        <form onSubmit={handleSubmit(sendEmbed)}>

            <FormFragmentEmbed form={props.form} defaultValues={state}/>


            <FormFragmentBaseSection title={'Pokemon Stats'}>
                <FormFieldEmbedTitle form={props.form} defaultValues={state} />

                <Grid item xs={12} md={3}>
                    <EmbedTextField select SelectProps={{native: true}}
                                    name={'species'}
                                    label={'Pokemon'}
                                    defaultValue={state.species}
                                    inputRef={register({})}
                    >
                        {Array.from({length: species.length}, (x,i) => i).map(mon =>
                            <option key={mon} value={mon}>{species[mon-1]}</option>
                        )}
                    </EmbedTextField>
                </Grid>

                <FormFieldStarCount form={props.form} defaultValue={state.star} />

                <FormFieldShiny form={props.form} defaultValue={state.shiny} />

                <FormFieldSpeciesURL form={props.form} defaultValue={state.speciesURL} />

                <FormFieldGender form={props.form} defaultValue={state.gender} />

                <FormFieldNature form={props.form} defaultValue={state.nature} />

                <Grid item xs={6} md={3}><EmbedTextField
                    name={'ability'}
                    label={'Ability'}
                    defaultValue={state.ability}
                    inputRef={register({})}
                /></Grid>

                <FormFieldHA form={props.form} defaultValue={state.ha}/>

                <FormFieldIVs form={props.form} defaultValues={state}/>

            </FormFragmentBaseSection>


            <FormFragmentHost form={props.form} defaultValues={state}/>

            <FormFieldSubmitButton />

            <Grid container spacing={2} className={classes.sectionGrid}>
                <Grid item xs={12}>
                    <Typography variant={'h5'}>Preview</Typography>
                    <Divider/>
                </Grid>

            </Grid>


        </form>

    );
}


import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";


export default function FormCustom(props) {

    const [state, setState] = React.useState({
        title: 'title',
        webhook: 'https://discordapp.com/api/webhooks/715369861563547650/5doolev263cZcXLKUcmOFWceJi8EqPWJrI0uf14HnGaAe8cVrK7axYaBYOKhiS637xf_',
        username: '',
        avatarURL: '',
    });

    const { handleSubmit, triggerValidation } = props.form;

    const sendEmbed = async () => {
        await triggerValidation();
    }

    return (
        <form onSubmit={handleSubmit(sendEmbed)}>
            <div>Custom Den</div>
            <Grid container spacing={2}>

            </Grid>
        </form>
    );
}


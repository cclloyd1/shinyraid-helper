import React, {useEffect} from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {useToasts} from "react-toast-notifications";
import Discord from 'discord.js';
import Grid from "@material-ui/core/Grid";
import EmbedTextField from "../EmbedTextField";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import {nests} from "../../resources/nests";
import {species} from "../../resources/species";
import {dens} from "../../resources/dens";
import {locations} from "../../resources/locations";
import Tooltip from "@material-ui/core/Tooltip";
//import {useTemplates} from "../../contexts/TemplatesProvider";
import {useTemplates} from "../TemplatesProvider";
import {useSettings} from "../../contexts/SettingsProvider";
import FormFragmentEmbed from "./fragments/FormFragmentEmbed";
import FormFragmentHost from "./fragments/FormFragmentHost";
import FormFragmentBaseSection from "./fragments/FormFragmentBaseSection";
import FormFieldHA from "./fields/FormFieldHA";
import FormFieldSpeciesURL from "./fields/FormFieldSpeciesURL";
import FormFieldShiny from "./fields/FormFieldShiny";
import FormFieldEmbedTitle from "./fields/FormFieldEmbedTitle";
import FormFieldSubmitButton from "./fields/FormFieldSubmitButton";
import {Controller} from "react-hook-form";


const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(2),
    },
    sectionGrid: {
        marginBottom: theme.spacing(3),
    },
}));

function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}



export default function FormRotating(props) {
    const { addToast } = useToasts();
    const { currentTemplate } = useTemplates();
    const { settings } = useSettings();
    const { register, handleSubmit, watch, setValue, control } = props.form;

    const [state, setState] = React.useState(currentTemplate.json);
    const [pokemon, setPokemon] = React.useState([]);
    const [currentSpecies, setCurrentSpecies] = React.useState({Stars: [true, true, true, true, true]});
    const [game, setGame] = React.useState('SwordEntries');

    const watchDen = watch('den', state.den);
    const watchRare = watch('rare', state.rare);
    const watchSpecies = watch('species', state.species);
    const watchStars = watch('star', state.star);
    const classes = useStyles();


    // Set the local game variable when settings are loaded/changed.
    useEffect(() => {
        //console.log('game', settings.game === 'sword' ? 'SwordEntries' : 'ShieldEntries');
        setGame(settings.game === 'sword' ? 'SwordEntries' : 'ShieldEntries');
    }, []);


    // Updates the currentSpecies state when the selection changes
    useEffect(() => {
        const den = dens[watchDen];
        if (watchRare === 'true') {
            for (let mon of nests[den.rare.hash][game]) {
                if (mon.Species === parseInt(watchSpecies))
                    setCurrentSpecies(mon);
            }
        }
        else {
            for (let mon of nests[den.common.hash][game]) {
                if (mon.Species === parseInt(watchSpecies))
                    setCurrentSpecies(mon);
            }
        }
    }, [watchSpecies]);


    // Updates the list of available pokemon when the den or rarity changes.
    useEffect(() => {
        const den = dens[watchDen];
        console.log('gameeffect', game, den.rare.hash);
        let table;
        if (watchRare === 'true')
            table = nests[den.rare.hash][game];
        else
            table = nests[den.common.hash][game];

        let possible = []
        for (let mon of table) {
            if (settings.baby) {
                if (mon.Stars[0] || mon.Stars[1] || mon.Stars[2])
                    possible.push(mon);
            }
            else {
                if (mon.Stars[2] || mon.Stars[3] || mon.Stars[4])
                    possible.push(mon);
            }
        }

        let isInCurrent = false;
        for (let mon of possible) {
            if (watchSpecies === mon.Species.toString()) {
                isInCurrent = true;
                break;
            }
        }
        if (!isInCurrent) {
            setValue('species', possible[0].Species);
        }


        setPokemon([...possible]);
    }, [watchDen, watchRare, game]);


    // Change value of stars field when species changes (in case it changes what stars are available)
    useEffect(() => {
        if (currentSpecies.Stars[parseInt(watchStars)-1] === false) {
            for (let i=0; i<currentSpecies.Stars.length; i++) {
                //console.log(i);
                if (currentSpecies.Stars[i]) {
                    setValue('star', i+1);
                    break;
                }
            }
        }
    }, [currentSpecies]);


    // Change the title and set the form values when the template is loaded.
    useEffect(() => {
        let values = [];
        for (let key in currentTemplate.json) {
            values.push({[key]: currentTemplate.json[key]});
        }
        setValue(values);
        document.title = `EmbedHelper · ${currentTemplate.label}`;
    }, [currentTemplate]);


    // Callback to send the embed.
    const sendEmbed = (data, e) => {
        let friendInfo = '';
        if (data.sn === data.ign)
            friendInfo += `**SN/IGN:** ${data.sn} │ `;
        else
            friendInfo += `**IGN:** ${data.ign} │ **SN:** ${data.sn} │ `;
        friendInfo += `${data.fc}`;

        let denNum;
        if (watchRare === 'true')
            denNum = dens[watchDen].rare.den;
        else
            denNum = dens[watchDen].common.den;

        let monURL;
        if (data.shiny === 'not')
            monURL = `https://www.serebii.net/swordshield/pokemon/${data.species.toString().padStart(3, '0')}${currentSpecies.IsGigantamax ? '-gi' : ''}.png`
        else
            monURL = `https://www.serebii.net/Shiny/SWSH/${data.species.toString().padStart(3, '0')}${currentSpecies.IsGigantamax ? '-gi' : ''}.png`
        if (data.speciesURL !== '')
            monURL = data.speciesURL;

        let gmax = '';
        if (currentSpecies.IsGigantamax)
            gmax = 'G-Max ';

        let possibleStr = '**Possible:** ';
        for (let mon of pokemon)
            possibleStr += `${mon.IsGigantamax ? 'G-Max ' : ''}${species[mon.Species-1]}, `;
        possibleStr = possibleStr.substr(0, possibleStr.length-2);

        let ha = '';
        if (data.ha)
            ha = '(HA)';

        let notes = ''
        if (data.discordID !== '')
            notes = `**Discord:** <@!${data.discordID}>\n`;
        notes += data.notes;

        let title = `Rotating Den ${denNum} │ ${capitalize(data.shiny)} Shiny │ Ability ${data.ability} ${data.ha ? '(HA)' : ''}`;
        if (data.embedTitle !== '') {
            if (data.embedTitleAppend === 'append')
                title = `${data.embedTitle} ${title}`;
            else if (data.embedTitleAppend === 'prepend')
                title = `${title} ${data.embedTitle}`;
            else title = data.embedTitle;
        }



        const embed = new Discord.MessageEmbed();
        embed.setAuthor(title, 'https://i.imgur.com/vuD73fM.png');
        embed.setDescription(`**Current:  ${'⭐'.repeat(data.star)}  ${gmax}${species[data.species-1]}**\n${possibleStr}`)
        embed.setThumbnail(monURL);
        embed.setColor(data.embedColor);
        embed.addField(`**Code:**`,
            `${data.code}`,
            true
        );
        embed.addField(friendInfo, notes, false );


        fetch(data.webhook, {
            method: 'POST',
            body: JSON.stringify({
                username: data.username,
                avatar_url: data.avatarURL,
                embeds: [embed.toJSON()],
            }),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(res => {
            if (res.ok)
                addToast('Sent embed!');
            else
                addToast('Error sending embed!', {appearance: 'error'});
        })
    }



    return (
        <form onSubmit={handleSubmit(sendEmbed)}>

            <FormFragmentEmbed form={props.form} defaultValues={state}/>


            <FormFragmentBaseSection title={'Den Info'}>

                <FormFieldEmbedTitle form={props.form} defaultValues={state} />

                <Grid item xs={6} md={4}>
                    <EmbedTextField select SelectProps={{native: true}}
                                    name={'den'}
                                    label={'Den #'}
                                    defaultValue={state.den}
                                    inputRef={register({})}
                    >
                        {Array.from(Array(dens.length).keys()).map(den =>
                            <option key={den} value={den} disabled={dens[den].common.hash === '0000000000000000'}>{den+1}: {locations[dens[den].location]}</option>
                        )}
                    </EmbedTextField>
                </Grid>
                <Tooltip title={'Number in parentheses is the Serebii den number.'} enterDelay={1000} leaveDelay={250} placement={'top'}>
                    <Grid item xs={6} md={3}>
                        <EmbedTextField select SelectProps={{native: true}}
                                        name={'rare'}
                                        label={'Rarity'}
                                        defaultValue={state.rare}
                                        inputRef={register({})}
                        >
                            <option value={false}>Common ({dens[watchDen].common.den})</option>
                            <option value={true}>Rare ({dens[watchDen].rare.den})</option>
                        </EmbedTextField>
                    </Grid>
                </Tooltip>

                <Grid item xs={12} md={3}>
                    <Controller
                        as={EmbedTextField}
                        label={'Pokemon'}
                        name="species"
                        defaultValue={state.species}
                        control={control}
                        select
                        SelectProps={{native: true}}
                    >
                        {pokemon.map((mon, i) => {
                            let starsStart = 1;
                            let starsEnd = 5;
                            let index = 0;
                            for (index = 0; index < mon.Stars.length; index++) {
                                if (mon.Stars[index]) {
                                    starsStart = index+1;
                                    break;
                                }
                            }
                            for (index; index < mon.Stars.length; index++) {
                                if (!mon.Stars[index]) {
                                    starsEnd = index;
                                    break;
                                }
                            }
                            let starsStr = '';
                            if (starsStart === starsEnd)
                                starsStr = `${starsStart}★`;
                            else
                                starsStr = `${starsStart}-${starsEnd}★`;
                            return (
                                <option key={`${mon.Species}-${i}`} value={mon.Species}>{mon.IsGigantamax ? 'G-Max ' : ''}{species[mon.Species - 1]}  {starsStr}</option>
                            );
                        })}
                    </Controller>
                </Grid>

                <Grid item xs={12} md={2}>
                    <Controller
                        as={EmbedTextField}
                        label={'Star Count'}
                        name="star"
                        defaultValue={parseInt(state.star) || 1}
                        control={control}
                        select
                        SelectProps={{native: true}}
                    >
                        <option value={1} disabled={!currentSpecies.Stars[0]}>{'★'.repeat(1)}</option>
                        <option value={2} disabled={!currentSpecies.Stars[1]}>{'★'.repeat(2)}</option>
                        <option value={3} disabled={!currentSpecies.Stars[2]}>{'★'.repeat(3)}</option>
                        <option value={4} disabled={!currentSpecies.Stars[3]}>{'★'.repeat(4)}</option>
                        <option value={5} disabled={!currentSpecies.Stars[4]}>{'★'.repeat(5)}</option>
                    </Controller>
                </Grid>

                <Grid item xs={6} md={2}>
                    <EmbedTextField select SelectProps={{native: true}}
                                    name={'ability'}
                                    label={'Ability'}
                                    defaultValue={state.ability}
                                    inputRef={register({})}
                    >
                        <option value={1}>Ability 1</option>
                        <option value={2}>Ability 2</option>
                        <option value={3}>Ability 3</option>
                    </EmbedTextField>
                </Grid>

                <FormFieldHA xs={2} form={props.form} defaultValue={state.ha} />

                <FormFieldShiny form={props.form} defaultValue={state.shiny} />

                <FormFieldSpeciesURL md={5} form={props.form} defaultValue={state.speciesURL} />

            </FormFragmentBaseSection>


            <FormFragmentHost form={props.form} defaultValues={state}/>


            <FormFieldSubmitButton />


            <Grid container spacing={2} className={classes.sectionGrid}>
                <Grid item xs={12}>
                    <Typography variant={'h5'}>Preview</Typography>
                    <Divider/>
                </Grid>

            </Grid>


        </form>

    );
}


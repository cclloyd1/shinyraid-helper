import React from 'react';
import Grid from "@material-ui/core/Grid";
import EmbedTextField from "../../EmbedTextField";
import FormFragmentBaseSection from "./FormFragmentBaseSection";


export default function FormFragmentHost(props: any) {
    const { register } = props.form;
    const { defaultValues } = props;


    return (
        <FormFragmentBaseSection title={'Raid Host Info'}>
            <Grid item xs={12} md={4}><EmbedTextField
                name={'code'}
                label={'Raid Code'}
                defaultValue={defaultValues.code}
                inputRef={register({})}
            /></Grid>

            <Grid item xs={12} md={2}><EmbedTextField
                name={'sn'}
                label={'Switch Name'}
                defaultValue={defaultValues.sn}
                inputRef={register({})}
            /></Grid>
            <Grid item xs={12} md={2}><EmbedTextField
                name={'ign'}
                label={'In-Game Name'}
                defaultValue={defaultValues.ign}
                inputRef={register({})}
            /></Grid>
            <Grid item xs={12} md={4}><EmbedTextField
                //onChange={handleTextChange}
                name={'fc'}
                label={'Friend Code'}
                defaultValue={defaultValues.fc}
                inputRef={register({})}
            /></Grid>

            <Grid item xs={12}><EmbedTextField
                multiline
                rows={4}
                name={'notes'}
                label={'Extra Info'}
                defaultValue={defaultValues.notes}
                inputRef={register({})}
            /></Grid>
        </FormFragmentBaseSection>
    );
}


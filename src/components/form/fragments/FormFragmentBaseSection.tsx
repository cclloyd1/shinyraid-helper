import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(3),
    },
}));

export default function FormFragmentBaseSection(props: any) {
    const classes = useStyles();

    return (
        <Grid container spacing={2} className={classes.root}>

            {props.title !== '' &&
                <Grid item xs={12}>
                    <Typography variant={'h5'}>{props.title}</Typography>
                    <Divider/>
                </Grid>
            }

            {props.children}

        </Grid>
    );
}

FormFragmentBaseSection.defaultProps = {
    title: '',
}


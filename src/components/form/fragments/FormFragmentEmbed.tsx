import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import {urlPattern} from "../../../App";
import EmbedTextField from "../../EmbedTextField";
// @ts-ignore
import {SketchPicker} from 'react-color';
import Dialog from "@material-ui/core/Dialog";
import InputAdornment from "@material-ui/core/InputAdornment";
import Box from "@material-ui/core/Box";
import FormFragmentBaseSection from "./FormFragmentBaseSection";

interface themeProps {
    embedColor: string;
}

const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: theme.spacing(3),
    },
    sectionGrid: {
        marginBottom: theme.spacing(3),
    },
    colorSwatch: (props: themeProps) => ({
        width: theme.spacing(5),
        height: 30,
        border: '1px solid black',
        borderRadius: theme.spacing(0.25),
        backgroundColor: props.embedColor,
    }),
}));


export default function FormFragmentEmbed(props: any) {
    const { register, errors, watch, setValue } = props.form;
    const { defaultValues } = props;

    const [colorPickerActive, setColorPickerActive] = React.useState<boolean>(false);
    const [colorPicker, setColorPicker] = React.useState<string>(defaultValues.embedColor);

    const watchColor = watch('embedColor', defaultValues.embedColor);
    const classes = useStyles({
        embedColor: watchColor,
    } as themeProps);

    const toggleColorPicker = () => {
        setColorPickerActive(!colorPickerActive);
    }


    return (
        <FormFragmentBaseSection title={'Bot Info'}>

            <Grid item xs={12}>
                <EmbedTextField
                    name={'webhook'}
                    label={'Webhook URL'}
                    defaultValue={defaultValues.webhook}
                    error={errors.webhook !== undefined}
                    helperText={errors.webhook && 'Not a valid webhook URL.'}
                    inputRef={register({
                        required: true,
                        pattern: /https:\/\/discordapp.com\/api\/webhooks\/(\d+)\/([A-za-z0-9_]+)/
                    })}
                />
            </Grid>

            <Grid item xs={6} md={4}>
                <EmbedTextField
                    name={'username'}
                    label={'Username'}
                    defaultValue={defaultValues.username}
                    error={errors.username !== undefined}
                    helperText={errors.username && 'Must be at least 2 characters.'}
                    inputRef={register({
                        minLength: 2,
                        min: 2,
                        required: true,
                    })}
                />
            </Grid>

            <Grid item xs={6} md={8}>
                <EmbedTextField
                    name={'avatarURL'}
                    label={'Avatar URL'}
                    defaultValue={defaultValues.avatarURL}
                    error={errors.avatarURL !== undefined}
                    helperText={errors.avatarURL && 'Not a valid URL.'}
                    inputRef={register({
                        pattern: urlPattern,
                    })}
                />
            </Grid>

            <Grid item xs={6} md={4}>
                <EmbedTextField
                    name={'discordID'} label={'Discord ID #'}
                    defaultValue={defaultValues.discordID}
                    inputRef={register({
                        minLength: 10,
                        min: 32,
                        pattern: /\d+/,
                    })}
                />
            </Grid>

            <Grid item xs={6} md={4}>
                <EmbedTextField
                    name={'embedColor'}
                    label={'Embed Color'}
                    defaultValue={colorPicker}
                    error={errors.embedColor !== undefined}
                    inputRef={register({})}
                    onClick={toggleColorPicker}
                    InputProps={{
                        endAdornment: <InputAdornment position="end"><Box className={classes.colorSwatch}/></InputAdornment>
                    }}
                />
                <Dialog open={colorPickerActive} onClose={toggleColorPicker}>
                    <SketchPicker
                        disableAlpha
                        color={colorPicker}
                        onChangeComplete={(color: any, e: any) => {
                            setColorPicker(color.hex);
                            setValue('embedColor', color.hex);
                        }}
                    />
                </Dialog>
            </Grid>

        </FormFragmentBaseSection>
    );
}


import React from 'react';
import Grid from "@material-ui/core/Grid";
import EmbedTextField from "../../EmbedTextField";


export default function FormFieldIVs(props: any) {
    const { register, errors } = props.form;
    const { defaultValues } = props;


    return (
        <>
            <Grid item xs={4} md={2}><EmbedTextField
                name={'ivhp'}
                label={'HP'}
                defaultValue={defaultValues.ivhp}
                error={errors.ivhp !== undefined}
                helperText={errors.ivhp && 'Must be 0-31 or ??'}
                inputRef={register({pattern: /^\d{1,2}$|^\?\?$/})}
            /></Grid>
            <Grid item xs={4} md={2}><EmbedTextField
                name={'ivatk'}
                label={'Atk'}
                defaultValue={defaultValues.ivatk}
                error={errors.ivatk !== undefined}
                helperText={errors.ivatk && 'Must be 0-31 or ??'}
                inputRef={register({pattern: /^\d{1,2}$|^\?\?$/})}
            /></Grid>
            <Grid item xs={4} md={2}><EmbedTextField
                name={'ivdef'}
                label={'Def'}
                defaultValue={defaultValues.ivdef}
                error={errors.ivdef !== undefined}
                helperText={errors.ivdef && 'Must be 0-31 or ??'}
                inputRef={register({pattern: /^\d{1,2}$|^\?\?$/})}
            /></Grid>
            <Grid item xs={4} md={2}><EmbedTextField
                name={'ivspatk'}
                label={'SpAtk'}
                defaultValue={defaultValues.ivspatk}
                error={errors.ivspatk !== undefined}
                helperText={errors.ivspatk && 'Must be 0-31 or ??'}
                inputRef={register({pattern: /^\d{1,2}$|^\?\?$/})}
            /></Grid>
            <Grid item xs={4} md={2}><EmbedTextField
                name={'ivspdef'}
                label={'SpDef'}
                defaultValue={defaultValues.ivspdef}
                error={errors.ivspdef !== undefined}
                helperText={errors.ivspdef && 'Must be 0-31 or ??'}
                inputRef={register({pattern: /^\d{1,2}$|^\?\?$/})}
            /></Grid>
            <Grid item xs={4} md={2}><EmbedTextField
                name={'ivspe'}
                label={'Spe'}
                defaultValue={defaultValues.ivspe}
                error={errors.ivspe !== undefined}
                helperText={errors.ivspe && 'Must be 0-31 or ??'}
                inputRef={register({pattern: /^\d{1,2}$|^\?\?$/})}
            /></Grid>
        </>
    );
}


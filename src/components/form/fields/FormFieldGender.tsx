import React from 'react';
import Grid from "@material-ui/core/Grid";
import EmbedTextField from "../../EmbedTextField";


export default function FormFieldShiny(props: any) {
    const { register } = props.form;
    const { defaultValue } = props;


    return (
        <Grid item xs={6} md={2}>
            <EmbedTextField
                select
                SelectProps={{native: true}}
                name={'gender'}
                label={'Gender'}
                defaultValue={defaultValue}
                inputRef={register({})}
            >
                <option value={'genderless'}>Genderless</option>
                <option value={'male'}>Male</option>
                <option value={'female'}>Female</option>
            </EmbedTextField>
        </Grid>
    );
}


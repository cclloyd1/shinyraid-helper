import React from 'react';
import Grid from "@material-ui/core/Grid";
import EmbedTextField from "../../EmbedTextField";


export default function FormFieldNature(props: any) {
    const { register } = props.form;
    const { defaultValue } = props;


    return (
        <Grid item xs={6} md={3}>
            <EmbedTextField
                select
                SelectProps={{native: true}}
                name={'nature'}
                label={'Nature'}
                defaultValue={defaultValue}
                inputRef={register({})}
        >
            <optgroup label="Other">
                <option value={'unknown'}>Unknown</option>
                <option value={'none'}>Don't Show</option>
            </optgroup>

            <optgroup label="Attack">
                <option value={'hardy'}>Hardy</option>
                <option value={'lonely'}>Lonely (+Atk/-Def)</option>
                <option value={'adamant'}>Adamant (+Atk/-SpAtk)</option>
                <option value={'naughty'}>Naughty (+Atk/-SpDef)</option>
                <option value={'brave'}>Brave (+Atk/-Spe)</option>
            </optgroup>
            <optgroup label="Defense">
                <option value={'bold'}>Bold (+Def/-Atk)</option>
                <option value={'docile'}>Docile</option>
                <option value={'impish'}>Impish (+Def/-SpAtk)</option>
                <option value={'lax'}>Lax (+Def/-SpDef)</option>
                <option value={'relaxed'}>Relaxed (+Def/-Spe)</option>
            </optgroup>
            <optgroup label="Special Attack">
                <option value={'modest'}>Modest (+SpAtk/-Atk)</option>
                <option value={'mild'}>Mild (+SpAtk/-Def)</option>
                <option value={'bashful'}>Bashful</option>
                <option value={'rash'}>Rash (+SpAtk/-SpDef)</option>
                <option value={'quiet'}>Quiet (+SpAtk/-Spe)</option>
            </optgroup>
            <optgroup label="Special Defense">
                <option value={'calm'}>Calm (+SpDef/-Atk)</option>
                <option value={'gentle'}>Gentle (+SpDef/-Def)</option>
                <option value={'careful'}>Careful (+SpDef/-SpAtk)</option>
                <option value={'quirky'}>Quirky</option>
                <option value={'sassy'}>Sassy (+SpDef/-Spe)</option>
            </optgroup>
            <optgroup label="Speed">
                <option value={'timid'}>Timid (+Spe/-Atk)</option>
                <option value={'hasty'}>Hasty (+Spe/-Def)</option>
                <option value={'jolly'}>Jolly (+Spe/-SpAtk)</option>
                <option value={'naive'}>Naive (+Spe/-SpDef)</option>
                <option value={'serious'}>Serious</option>
            </optgroup>
        </EmbedTextField></Grid>
    );
}


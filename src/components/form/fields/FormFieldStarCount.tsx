import React from 'react';
import Grid from "@material-ui/core/Grid";
import EmbedTextField from "../../EmbedTextField";


export default function FormFieldStarCount(props: any) {
    const { register } = props.form;
    const { defaultValue, xs, md } = props;


    return (
        <Grid item xs={xs} md={md}>
            <EmbedTextField
                select
                SelectProps={{native: true}}
                name={'star'}
                label={'Star Count'}
                defaultValue={defaultValue}
                inputRef={register({})}
            >
                <option value={1}>★</option>
                <option value={2}>★★</option>
                <option value={3}>★★★</option>
                <option value={4}>★★★★</option>
                <option value={5}>★★★★★</option>
            </EmbedTextField>
        </Grid>
    );
}

FormFieldStarCount.defaultProps = {
    xs: 6,
    md: 2,
}


import React from 'react';
import Grid from "@material-ui/core/Grid";
import Tooltip from "@material-ui/core/Tooltip";
import EmbedTextField from "../../EmbedTextField";


export default function FormFieldEmbedTitle(props: any) {
    const { register } = props.form;
    const { defaultValues } = props;


    return (
        <>
            <Tooltip title={'This will override the title of the embed.'} enterDelay={1000} leaveDelay={250} placement={'top'}>
                <Grid item xs={12} md={8}><EmbedTextField
                    name={'embedTitle'}
                    label={'(Optional) Custom Title'}
                    defaultValue={defaultValues.embedTitle}
                    placeholder={'(Optional) Custom Title'}
                    inputRef={register({})}
                /></Grid>
            </Tooltip>
            <Tooltip title={'Append/Prepend generated title to custom title.'} enterDelay={1000} leaveDelay={250} placement={'top'}>
                <Grid item xs={12} md={4}>
                    <EmbedTextField
                        name={'embedTitleAppend'}
                        label={'Append/Prepend Title'}
                        defaultValue={defaultValues.embedTitleReplace || 'replace'}
                        inputRef={register({})}
                        select
                        SelectProps={{native: true}}
                    >
                        <option value={'replace'}>Replace</option>
                        <option value={'prepend'}>Prepend</option>
                        <option value={'append'}>Append</option>
                    </EmbedTextField>
                </Grid>
            </Tooltip>
        </>
    );
}


import React from 'react';
import Grid from "@material-ui/core/Grid";
import EmbedTextField from "../../EmbedTextField";


export default function FormFieldShiny(props: any) {
    const { register } = props.form;
    const { defaultValue } = props;


    return (
        <Grid item xs={6} md={2}>
            <EmbedTextField
                select
                SelectProps={{native: true}}
                name={'shiny'}
                label={'Shiny'}
                defaultValue={defaultValue.shiny}
                inputRef={register({})}
            >
                <option value={'star'}>Star</option>
                <option value={'square'}>Square</option>
                <option value={'not'}>Not Shiny</option>
            </EmbedTextField>
        </Grid>
    );
}


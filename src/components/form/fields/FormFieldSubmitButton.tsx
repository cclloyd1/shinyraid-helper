import React from 'react';
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import FormFragmentBaseSection from "../fragments/FormFragmentBaseSection";


export default function FormFieldSubmitButton() {
    return (
        <FormFragmentBaseSection>
            <Grid item xs={12}>
                <Button variant={'contained'} color={'primary'} type="submit" fullWidth size={'large'}>Send Embed</Button>
            </Grid>
        </FormFragmentBaseSection>
    );
}


import React from 'react';
import Grid from "@material-ui/core/Grid";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
}));


export default function FormFieldHA(props: any) {
    const classes = useStyles();
    const { register } = props.form;
    const { defaultValue, xs, md } = props;


    return (
        <Grid item xs={xs} md={md} className={classes.root}>
            <FormControlLabel
                control={
                    <Checkbox
                        defaultChecked={defaultValue}
                        name={'ha'}
                        color="primary"
                        inputRef={register({})}
                    />
                }
                label="Hidden Ability?"
            />
        </Grid>
    );
}

FormFieldHA.defaultProps = {
    xs: 6,
    md: 3,
}


import React from 'react';
import Grid from "@material-ui/core/Grid";
import Tooltip from "@material-ui/core/Tooltip";
import EmbedTextField from "../../EmbedTextField";

export default function FormFieldSpeciesURL(props: any) {
    const { register } = props.form;
    const { defaultValue, xs, md } = props;


    return (
        <Tooltip title={'By default it will use images from serebii.'} enterDelay={1000} leaveDelay={250} placement={'top'}>
            <Grid item xs={xs} md={md}>
                <EmbedTextField
                    name={'speciesURL'}
                    label={'(Optional) Image URL'}
                    defaultValue={defaultValue}
                    placeholder={'(Optional) Override Image'}
                    inputRef={register({})}
                />
            </Grid>
        </Tooltip>
    );
}

FormFieldSpeciesURL.defaultProps = {
    xs: 12,
    md: 5,
}



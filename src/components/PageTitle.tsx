import * as React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import Box from "@material-ui/core/Box";
import PropTypes from "prop-types";


const useStyles = makeStyles(theme => ({
    pageTitleDivider: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
}));

export default function PageTitle(props: any) {
    const classes = useStyles();
    const {title, children} = props;

    return (
        <Box>
            <Typography variant="h2">{title}</Typography>
            {children}
            <Divider className={classes.pageTitleDivider}/>
        </Box>
    );
}

PageTitle.defaultProps = {
    title: '',
    children: null,
};

PageTitle.propTypes = {
    title: PropTypes.node.isRequired,
    children: PropTypes.node,
};

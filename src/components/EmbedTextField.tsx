import TextField from "@material-ui/core/TextField";
import React from "react";


export default function EmbedTextField(props: any) {
    return (
        <TextField
            variant={'outlined'}
            fullWidth
            autoComplete={'off'}
            {...props}
        >
            {props.children}
        </TextField>
    )
}
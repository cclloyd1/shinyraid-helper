import * as React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import PropTypes from 'prop-types';


const useStyles = makeStyles(theme => ({
    question: {
        marginBottom: theme.spacing(1),
    },
    answer: {
        marginBottom: theme.spacing(1),
    },
    divider: {
        margin: theme.spacing(3, 0),
    },
}));

export default function FAQItem(props: any) {
    const classes = useStyles();

    return (
        <>
            <Typography variant={'h5'} className={classes.question}>{props.question}</Typography>
            <Typography variant={'body1'} className={classes.answer}>{props.answer}</Typography>
            <Divider className={classes.divider}/>
        </>
    );
}

FAQItem.defaultProps = {
    question: '',
    answer: '',
};

FAQItem.propTypes = {
    question: PropTypes.node.isRequired,
    answer: PropTypes.node.isRequired,
};
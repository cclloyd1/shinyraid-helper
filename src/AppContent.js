import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import {Route, Switch} from "react-router-dom";
import PageHome from "./pages/PageHome";
import {drawerWidth} from "./theme";
import {useGlobal} from "reactn";
import clsx from "clsx";
import PageSettings from "./pages/PageSettings";
import PageToolIVCalculator from "./pages/PageToolIVCalculator";
import PageToolEncounterLookup from "./pages/PageToolEncounterLookup";
import PageHelp from "./pages/PageHelp";
import PageHost from "./pages/PageHost";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.mixins.toolbar.minHeight + theme.spacing(1),
        flexGrow: 1,
    },
    content: {
        marginLeft: 0,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    contentShift: {
        marginLeft: drawerWidth,
        transition: theme.transitions.create('margin', {
            easing: theme.transitions.easing.easeOut,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
}));


export default function AppContent() {
    const classes = useStyles();

    const [drawerOpen] = useGlobal('drawerOpen');


    return (
        <main className={clsx(classes.root, classes.content, {
            [classes.contentShift]: drawerOpen && window.innerWidth >= 960,
        })}>
            <Container maxWidth="md">
                <Switch>
                    <Route path='/' exact><PageHome/></Route>
                    <Route path='/host' exact><PageHost/></Route>
                    <Route path='/settings' exact><PageSettings/></Route>
                    <Route path='/help' exact><PageHelp/></Route>
                    <Route path='/tools/ivcalc' exact><PageToolIVCalculator/></Route>
                    <Route path='/tools/encounterlookup' exact><PageToolEncounterLookup/></Route>
                </Switch>
            </Container>
        </main>

    );
}


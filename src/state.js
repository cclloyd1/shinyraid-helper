
export const INITIAL_STATE = {
    auth: {
        access: null,
        refresh: null,
        errors: null,
    },
    drawerOpen: window.innerWidth >= 960,
    formDirty: false,
};

export const INITIAL_REDUCERS = {
    addOne: ({ count }) => ({
        count: count + 1,
    }),
};
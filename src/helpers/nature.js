import capitalize from "@material-ui/core/utils/capitalize";

export const getFullNature = nature => {
    switch (nature) {
        case 'lonely': return 'Lonely (+Atk/-Def)';
        case 'adamant': return 'Adamant (+Atk/-SpAtk)';
        case 'naughty': return 'Naughty (+Atk/-SpDef)';
        case 'brave': return 'Brave (+Atk/-Spe)';

        case 'bold': return 'Bold (+Def/-Atk)';
        case 'impish': return 'Impish (+Def/-SpAtk)';
        case 'lax': return 'Lax (+Def/-SpDef)';
        case 'relaxed': return 'Relaxed (+Def/-Spe)';

        case 'modest': return 'Modest (+SpAtk/-Atk)';
        case 'mild': return 'Mild (+SpAtk/-Def)';
        case 'rash': return 'Rash (+SpAtk/-SpDef)';
        case 'quiet': return 'Quiet (+SpAtk/-Spe)';

        case 'calm': return 'Calm (+SpDef/-Atk)';
        case 'gentle': return 'Gentle (+SpDef/-Def)';
        case 'careful': return 'Careful (+SpDef/-SpAtk)';
        case 'sassy': return 'Sassy (+SpDef/-Spe)';

        case 'timid': return 'Timid (+Spe/-Atk)';
        case 'hasty': return 'Hasty (+Spe/-Def)';
        case 'jolly': return 'Jolly (+Spe/-SpAtk)';
        case 'naive': return 'Naive (+Spe/-SpDef)';

        default: return capitalize(nature);
    }
}
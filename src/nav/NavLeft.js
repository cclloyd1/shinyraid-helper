import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import {useGlobal} from 'reactn';
import {appBarHeight, drawerWidth} from "../theme";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import Drawer from "@material-ui/core/Drawer";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
import ListItemText from "@material-ui/core/ListItemText";
//import {useTemplates} from "../contexts/TemplatesProvider";
import {useTemplates} from "../components/TemplatesProvider";
import ListSubheader from "@material-ui/core/ListSubheader";
import {Link} from 'react-router-dom';
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles(theme => ({
    root: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    drawerHeader: {
        display: 'flex',
        alignItems: 'center',
        padding: theme.spacing(0, 1),
        // necessary for content to be below app bar
        //...theme.mixins.toolbar,
        height: appBarHeight,
        justifyContent: 'flex-end',
    },
    nested: {
        paddingLeft: theme.spacing(4),
    },
    link: {
        color: 'inherit',
        textDecoration: 'none',
    }
}));

export default function NavTop() {
    const classes = useStyles();

    const { resetTemplate } = useTemplates();

    const [drawerOpen, setDrawerOpen] = useGlobal('drawerOpen');


    const toggleDrawer = async () => {
        await setDrawerOpen(!drawerOpen);
    };

    const handleResetTemplate = () => {
        resetTemplate();
    }


    return (
        <Drawer
            className={classes.root}
            variant={window.innerWidth >= 960 ? "persistent" : "temporary"}
            anchor="left"
            open={drawerOpen}
            classes={{
                paper: classes.drawerPaper,
            }}
        >
            <div className={classes.drawerHeader}>
                <IconButton onClick={toggleDrawer}><ChevronLeftIcon /></IconButton>
            </div>
            <Divider/>
            <List>
                <Link to={'/'} className={classes.link}>
                    <ListItem button><ListItemText primary={'Home'} onClick={handleResetTemplate}/></ListItem>
                </Link>
                <Link to={'/settings'} className={classes.link}>
                    <ListItem button><ListItemText primary={'Settings'} /></ListItem>
                </Link>
                <Link to={'/help'} className={classes.link}>
                    <ListItem button><ListItemText primary={'Help'} /></ListItem>
                </Link>

                <ListSubheader>Tools</ListSubheader>
                <Link to={'/tools/ivcalc'} className={classes.link}>
                    <ListItem button className={classes.nested}><ListItemText primary="IV Calculator" /></ListItem>
                </Link>
                <Link to={'/tools/encounterlookup'} className={classes.link}>
                    <ListItem button className={classes.nested}><ListItemText primary="Encounter Lookup" /></ListItem>
                </Link>
            </List>
        </Drawer>
    );
}
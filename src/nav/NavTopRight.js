import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Box from "@material-ui/core/Box";
import RaidSelectDropdown from "../components/RaidSelectDropdown";

const useStyles = makeStyles(theme => ({
    root: {},
}));


export default function NavTopRight() {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            <RaidSelectDropdown />
        </Box>
    );
}
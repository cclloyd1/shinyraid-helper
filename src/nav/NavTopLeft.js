import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Box from "@material-ui/core/Box";
import MenuIcon from '@material-ui/icons/Menu';
import IconButton from "@material-ui/core/IconButton";
import {useGlobal} from "reactn";

const useStyles = makeStyles(theme => ({
    root: {},
    navLink: {
        color: '#fff',
        textDecoration: 'none',
    },
    menuContent: {
        padding: 0,
    },
    menuContentBox: {
        padding: theme.spacing(2),
        minWidth: 600,
    },
    dialogActions: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    description: {
        paddingTop: theme.spacing(4),
    },
    hidden: {
        display: 'none',
    }
}));


export default function NavTopLeft() {
    const classes = useStyles();

    const [drawerOpen, setDrawerOpen] = useGlobal('drawerOpen');

    const toggleDrawer = async () => {
        await setDrawerOpen(!drawerOpen);
    };


    return (
        <Box>
            <IconButton onClick={toggleDrawer}>
                <MenuIcon />
            </IconButton>
        </Box>
    );
}